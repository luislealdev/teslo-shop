import { Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { PaginationDto } from 'src/common/dtos/pagination.dto';

@Injectable()
export class ProductsService {

  private readonly logger = new Logger("ProductsService");

  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>
  ) { }

  async create(createProductDto: CreateProductDto) {
    try {
      const product = this.productRepository.create(createProductDto);
      await this.productRepository.save(product);
      return product;

    } catch (error) {
      this.handleExeptions(error)
    }
  }

  async findAll(paginationDto: PaginationDto) {

    const { limit = 10, offset = 0 } = paginationDto;

    try {
      const products = await this.productRepository.find({
        skip: offset,
        take: limit,
      });
      return products;
    } catch (error) {
      this.handleExeptions(error)
    }
  }

  async findOne(id: string) {

    const product = await this.productRepository.findOneBy({ id });

    if (!product) {
      throw new NotFoundException("Product not found");
    }

    return product;

  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  async remove(id: string) {
    try {
      await this.productRepository.delete({ id });
      return { message: "Product deleted" }
    } catch (error) {
      this.handleExeptions(error)
    }
  }


  private handleExeptions(error: any) {
    if (error.code === '23505') {
      throw new InternalServerErrorException(error.detail)
    }

    this.logger.error(error);
    throw new InternalServerErrorException("Unexpected error, check server logs");
  }
}
