import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from "typeorm";


// This is a postgres table
@Entity()
export class Product {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    // Add product title, it has to be text and unique
    @Column('text', { unique: true })
    title: string;

    @Column('float', {
        default: 0
    })
    price: number;

    @Column('text', {
        nullable: true
    })
    description: string;

    @Column('text', {
        unique: true
    })
    slug: string;

    @Column('int', {
        default: 0
    })
    stock: number;

    @Column('text', {
        array: true
    })
    sizes: string[]

    @Column('text', {
    })
    gender: string;

    //TODO: ADD TAGS

    //TODO: ADD IMAGES


    @BeforeInsert()
    checkSlugInsert() {
        if (!this.slug) {
            this.slug = this.title;
        }

        this.slug = this.slug.toLowerCase().replaceAll(/ /g, '-').replaceAll(/[^\w-]+/g, '');
    }

    //TODO: FINISH IT
    //@BeforeUpdate()
}
